package com.revolut.backend.test.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * POJO file for User and balance info
 */
public class User {

    private long id;
    private String name;
    private String currency;
    private long balance;

    @JsonCreator
    public User(
            @JsonProperty("id") long id,
            @JsonProperty("name") String name,
            @JsonProperty("currency") String currency,
            @JsonProperty("balance") long balance) {
        this.id = id;
        this.name = name;
        this.currency = currency;
        this.balance = balance;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }
}
