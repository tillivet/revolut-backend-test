package com.revolut.backend.test.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * POJO file for Money Transfer entity
 */
public class MoneyTransfer {

    private long transferId;
    private String currency;
    private long amount;
    private long senderId;
    private long recipientId;

    private String description;

    @JsonCreator
    public MoneyTransfer(
            @JsonProperty("currency") String currency,
            @JsonProperty("amount") long amount,
            @JsonProperty("senderId") long senderId,
            @JsonProperty("recipientId") long recipientId,
            @JsonProperty("description") String description) {
        this.currency = currency;
        this.amount = amount;
        this.senderId = senderId;
        this.recipientId = recipientId;
        this.description = description;
    }

    public long getTransferId() {
        return transferId;
    }

    public void setTransferId(long transferId) {
        this.transferId = transferId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public long getSenderId() {
        return senderId;
    }

    public void setSenderId(long senderId) {
        this.senderId = senderId;
    }

    public long getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(long recipientId) {
        this.recipientId = recipientId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoneyTransfer transfer = (MoneyTransfer) o;

        if (transferId != transfer.transferId) return false;
        if (Double.compare(transfer.amount, amount) != 0) return false;
        if (senderId != transfer.senderId) return false;
        if (recipientId != transfer.recipientId) return false;
        if (!currency.equals(transfer.currency)) return false;
        return description != null ? description.equals(transfer.description) : transfer.description == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = (int) (transferId ^ (transferId >>> 32));
        result = 31 * result + currency.hashCode();
        temp = Double.doubleToLongBits(amount);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (int) (senderId ^ (senderId >>> 32));
        result = 31 * result + (int) (recipientId ^ (recipientId >>> 32));
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }
}
