package com.revolut.backend.test.dao;

/**
 * Currency converter helper class.
 * For simplicity, uses 1 to 1 conversion
 */
public class CurrencyConverter {

    /**
     * Converts amount from originalCurrency to desiredOne
     *
     * @param originalCurrency original (from) currency
     * @param amount amount to be converted
     * @param desiredCurrency desired (to) currency
     * @return converted amount (we consider that fraction digits are handled)
     */
    public long convert(String originalCurrency, long amount, String desiredCurrency) {
        return amount * getCurrencyExchangeRate(originalCurrency, desiredCurrency);
    }

    /**
     * Gets currency convertion rates. In real world would use some database or external service provider
     *
     * @param from currency from
     * @param to currency to
     * @return converted amount
     */
    private long getCurrencyExchangeRate(String from, String to) {
        return 1;
    }
}
