package com.revolut.backend.test.dao;

import com.revolut.backend.test.model.User;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * In-memory dao. Stores users and their account balance
 */
public class UserDao {

    private ConcurrentMap<Long, User> users;

    /**
     * Create some initial data to play with
     */
    public UserDao() {
        users = new ConcurrentHashMap<>();
        users.put(123L, new User(123L, "Angela Merkel", "EUR", 100000L));
        users.put(321L, new User(321L, "Theresa May", "GBP", 10L));
    }

    /**
     *
     * @param user
     * @return
     */
    public boolean add(User user) {
        if (user == null || user.getId() == 0)
            return false;
        users.put(user.getId(), user);
        return true;
    }

    /**
     * Gets user info
     *
     * @param userId user id
     * @return user info
     */
    public User get(long userId) {
        return users.get(userId);
    }

    /**
     * Updates balance for a user
     *
     * @param userId user id
     * @param amount amount (in user's account currency)
     * @param incoming if true - add amount to the balance, if false - minus
     * @return true in case of success
     */
    public boolean updateBalance(long userId, long amount, boolean incoming) {
        User userToUpdate = users.get(userId);
        // if we add
        if (incoming) {
            userToUpdate.setBalance(userToUpdate.getBalance() + amount);
        } else {
            if (userToUpdate.getBalance() - amount < 0)
                return false;
            userToUpdate.setBalance(userToUpdate.getBalance() - amount);
        }
        return true;
    }
}
