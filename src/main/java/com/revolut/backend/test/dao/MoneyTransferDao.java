package com.revolut.backend.test.dao;

import com.revolut.backend.test.model.MoneyTransfer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * In-memory dao. Stores transfers by transfer ID
 * As transfer ID we'll use current timestamp in milliseconds
 */
public class MoneyTransferDao {

    private ConcurrentMap<Long, MoneyTransfer> transfers;

    public MoneyTransferDao() {
        this.transfers = new ConcurrentHashMap<>();
    }

    /**
     * For now, this method will allways return transfer id
     * (as we don't have any DB here and we'll always succeed saving it)
     *
     * @param moneyTransfer money transfer object to be saved
     * @return transaction id if transfer was saved
     */
    public long add(MoneyTransfer moneyTransfer) {
        long transferId = System.currentTimeMillis();
        moneyTransfer.setTransferId(transferId);
        // we add all of them, don't check if transfer already exists or not
        transfers.put(transferId, moneyTransfer);
        return transferId;
    }

    /**
     * Removes the transfer from the list
     *
     * @param transferId transfer id
     * @return true if it was deleted, false if not
     */
    public boolean delete(long transferId) {
        if (transfers.get(transferId) != null) {
            transfers.remove(transferId);
            return true;
        }
        return false;
    }

    /**
     * Gets transfer by transfer id
     *
     * @param transferId transfer id to search for
     * @return money transfer details
     */
    public MoneyTransfer get(long transferId) {
        return transfers.get(transferId);
    }

    /**
     * @return get list of all transactions
     */
    public List<MoneyTransfer> getAll() {
        return new ArrayList<>(transfers.values());
    }
}
