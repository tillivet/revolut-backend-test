package com.revolut.backend.test;


import com.revolut.backend.test.handlers.MoneyTransferHandler;
import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.server.handlers.BlockingHandler;

/**
 * Main class that runs undertow server and registers routes
 */
public class Runner {

    public static void main(String[] args) {

        Undertow server = Undertow.builder()
                .addHttpListener(8080, "localhost")
                .setHandler(Handlers.routing()
                        .get("/", MoneyTransferHandler::getAll)
                        .post("/transfer", MoneyTransferHandler::add)
                        .delete("/transfer/{id}", MoneyTransferHandler::delete)
                        .get("/transfer/{id}", MoneyTransferHandler::get)
                        .setFallbackHandler(MoneyTransferHandler::notFound))
                .build();
        server.start();
    }
}