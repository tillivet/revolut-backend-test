package com.revolut.backend.test.handlers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.revolut.backend.test.dao.CurrencyConverter;
import com.revolut.backend.test.dao.MoneyTransferDao;
import com.revolut.backend.test.dao.UserDao;
import com.revolut.backend.test.model.MoneyTransfer;
import com.revolut.backend.test.model.User;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;
import io.undertow.util.StatusCodes;

import java.io.IOException;
import java.util.Deque;
import java.util.List;

public class MoneyTransferHandler {

    private static final MoneyTransferDao dao = new MoneyTransferDao();
    private static final ObjectMapper mapper = new ObjectMapper();
    private static final UserDao userDao = new UserDao();

    /**
     * Deletes transfer by id if possible
     *
     * @param exchange contains request & performs response
     */
    public static void delete(HttpServerExchange exchange) {
        boolean deleted = dao.delete(getIdPathParam(exchange));
        if (deleted) {
            exchange.getResponseSender().send("Deleted");
        } else {
            notFound(exchange);
        }
    }


    /**
     * Get transfer details by id
     *
     * @param exchange contains request & performs response
     * @throws JsonProcessingException
     */
    public static void get(HttpServerExchange exchange) throws JsonProcessingException {
        MoneyTransfer transfer = dao.get(getIdPathParam(exchange));
        if (transfer != null) {
            exchange.getResponseSender().send(mapper.writeValueAsString(transfer));
        } else {
            notFound(exchange);
        }
    }

    /**
     * Add new transfer
     *
     * @param exchange contains request & performs response
     * @throws IOException
     */
    public static void add(HttpServerExchange exchange) throws IOException {
        exchange.getRequestReceiver().receiveFullString(MoneyTransferHandler::handleAdd);
    }


    /**
     * Get all REST API Handler
     *
     * @param exchange contains request & performs response
     * @throws JsonProcessingException
     */
    public static void getAll(HttpServerExchange exchange) throws JsonProcessingException {
        List<MoneyTransfer> all = dao.getAll();
        exchange.getResponseSender().send(mapper.writeValueAsString(all));
    }

    /**
     * 404 Not found handler
     *
     * @param exchange contains request & performs response
     */
    public static void notFound(HttpServerExchange exchange) {
        exchange.setStatusCode(StatusCodes.NOT_FOUND);
        exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "text/plain");
        exchange.getResponseSender().send("Not Found");
    }

    /**
     * Private util method to extract transfer id from path params
     */
    private static Long getIdPathParam(HttpServerExchange exchange) {
        Deque<String> params = exchange.getQueryParameters().get("id");
        if (params == null || params.size() == 0) {
            return Long.MIN_VALUE;
        }
        String transferId = params.getFirst();
        return Long.parseLong(transferId);
    }

    /**
     * Private method callback for adding money transfer
     * Note: We currently use XNIO in undertow, so we don't need transaction here in the most simple case
     * It would be nice to have some kind of transaction here
     */
    private static void handleAdd(HttpServerExchange exchange, String transferToAdd) {
        try {
            MoneyTransfer transfer = mapper.readValue(transferToAdd, MoneyTransfer.class);
            long transferId = performMoneyTransfer(transfer);
            exchange.setStatusCode(StatusCodes.CREATED);
            exchange.getResponseSender().send(Long.toString(transferId));
        } catch (IllegalArgumentException e) {
            exchange.setStatusCode(StatusCodes.NOT_FOUND);
            exchange.getResponseSender().send(e.getMessage());
        } catch (IllegalStateException e) {
            exchange.setStatusCode(StatusCodes.CONFLICT);
            exchange.getResponseSender().send(e.getMessage());
        } catch (IOException e) {
            exchange.setStatusCode(StatusCodes.INTERNAL_SERVER_ERROR);
            exchange.getResponseSender().close();
        }
    }

    /**
     * Execute actual logic for money transfer
     *
     * @param transfer transfer dto
     * @return transfer id
     */
    private static long performMoneyTransfer(MoneyTransfer transfer) {
        // 1. Check if user that sends transfer exists
        User sender = userDao.get(transfer.getSenderId());
        if (sender == null) throw new IllegalArgumentException("Sender not found!");
        // 2. TODO Check if user who requests transaction is the same user who makes transfer
        // 3. Check if user has enough money to make a transaction
        long amountInSenderCur = new CurrencyConverter().convert(
                transfer.getCurrency(), transfer.getAmount(), sender.getCurrency());
        if (sender.getBalance() < amountInSenderCur)
            throw new IllegalStateException("You don't have enough money!");
        // 4. update senders balance
        boolean success = userDao.updateBalance(sender.getId(), amountInSenderCur, false);
        if (!success)
            throw new IllegalStateException("You don't have enough money!");
        // 5. save transfer
        long transferId = dao.add(transfer);
        // 6. update recepients balance
        userDao.updateBalance(transfer.getRecipientId(), transfer.getAmount(), true);
        // 7. return transfer id
        return transferId;
    }
}
