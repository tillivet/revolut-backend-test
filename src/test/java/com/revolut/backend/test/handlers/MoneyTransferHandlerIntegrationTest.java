package com.revolut.backend.test.handlers;

import io.undertow.Handlers;
import io.undertow.Undertow;
import org.junit.*;
import org.junit.runners.MethodSorters;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import static org.junit.Assert.*;

/**
 * Integration test for the REST API
 * Running real undertow container + using resteasy to make calls to it
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MoneyTransferHandlerIntegrationTest {

    private static Undertow server;
    private static Client client;

    private static int port = 8080;
    private static String host = "localhost";
    private static String HOST_URL = "http://" + host + ":" + port;

    private String TRANSFER_EXAMPLE_SUCCESS =
            "{\"currency\": \"EUR\", \"amount\": \"100\",\"senderId\":\"123\", \"recipientId\":\"321\", \"description\": \"From Angela\"}";
    private String TRANSFER_EXAMPLE_USER_NOT_FOUND =
            "{\"currency\": \"GBP\", \"amount\": \"100\",\"senderId\":\"1\", \"recipientId\":\"123\", \"description\": \"From nobody\"}";
    private String TRANSFER_EXAMPLE_BALANCE_SMALL =
            "{\"currency\": \"GBP\", \"amount\": \"1000\",\"senderId\":\"321\", \"recipientId\":\"123\", \"description\": \"From Theresa\"}";
    private String EXPECTED_RESPONSE_START =
            "{\"currency\":\"EUR\",\"amount\":100,\"senderId\":123,\"recipientId\":321,\"description\":\"From Angela\",\"transferId\":";

    @BeforeClass
    public static void start() {
        // we need to have server instance to stop it at the end
        server = Undertow.builder()
                .addHttpListener(port, host)
                .setHandler(Handlers.routing()
                        .get("/", MoneyTransferHandler::getAll)
                        .post("/transfer", MoneyTransferHandler::add)
                        .delete("/transfer/{id}", MoneyTransferHandler::delete)
                        .get("/transfer/{id}", MoneyTransferHandler::get)
                        .setFallbackHandler(MoneyTransferHandler::notFound))
                .build();
        server.start();
        client = ClientBuilder.newClient();
    }

    @AfterClass
    public static void stop() {
        // we have to stop the server here -> 1 port binding may not be released
        server.stop();
    }

    @Test
    public void getAllEmpty() throws Exception {
        Response response = client.target(HOST_URL).request().get();
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        String val = response.readEntity(String.class);
        assertEquals("[]", val);
    }

    @Test
    public void delete() throws Exception {
        long transferId = addTransfer();

        Response response = client.target(HOST_URL + "/transfer/" + transferId)
                .request().delete();
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
    }

    @Test
    public void getTransferSuccess() throws Exception {
        long transferId = addTransfer();

        Response response = client.target(HOST_URL + "/transfer/" + transferId)
                .request().get();
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        String json = response.readEntity(String.class);
        assertTrue(json != null);
        // transfer id cannot be checked exactly, we check all but it
        assertTrue(json.startsWith(EXPECTED_RESPONSE_START));
    }

    @Test
    public void addTransferUserNotFound() throws Exception {
        Response response = client.target(HOST_URL + "/transfer")
                .request().buildPost(Entity.json(TRANSFER_EXAMPLE_USER_NOT_FOUND)).invoke();
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }

    @Test
    public void addTransferBalanceIsLow() throws Exception {
        Response response = client.target(HOST_URL + "/transfer")
                .request().buildPost(Entity.json(TRANSFER_EXAMPLE_BALANCE_SMALL)).invoke();
        assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
    }

    @Test
    public void getEmpty() throws Exception {
        Response response = client.target(HOST_URL + "/transfer/" + System.currentTimeMillis())
                .request().get();
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }

    private long addTransfer() throws Exception {
        Response response = client.target(HOST_URL + "/transfer")
                .request().buildPost(Entity.json(TRANSFER_EXAMPLE_SUCCESS)).invoke();
        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

        long transferId = Long.parseLong(response.readEntity(String.class));
        assertTrue(transferId <= System.currentTimeMillis());
        return transferId;
    }

    @Test
    public void getAllNotEmpty() throws Exception {
        addTransfer();
        addTransfer();
        Response response = client.target(HOST_URL).request().get();
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        String val = response.readEntity(String.class);
        assertTrue(val != null);
        assertTrue(!val.isEmpty());
        assertNotEquals("[]", val);
    }

    @Test
    public void notFound() throws Exception {
        Response response = client.target(HOST_URL + "/something")
                .request().get();
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }
}
