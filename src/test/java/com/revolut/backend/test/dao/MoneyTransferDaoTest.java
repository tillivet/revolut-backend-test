package com.revolut.backend.test.dao;

import com.revolut.backend.test.model.MoneyTransfer;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class MoneyTransferDaoTest {

    @Test
    public void add() throws Exception {
        MoneyTransferDao dao = new MoneyTransferDao();
        long transferId = dao.add(
                new MoneyTransfer("EUR", 120, 1234, 3214, "My first transfer"));
        assertTrue(transferId <= System.currentTimeMillis());
    }

    @Test
    public void delete() throws Exception {
        MoneyTransferDao dao = new MoneyTransferDao();
        long transferId = dao.add(
                new MoneyTransfer("EUR", 120, 1234, 3214, "My first transfer"));
        assertTrue(transferId <= System.currentTimeMillis());
        boolean deleted = dao.delete(transferId);
        assertTrue(deleted);
    }

    @Test
    public void failedDelete() throws Exception {
        MoneyTransferDao dao = new MoneyTransferDao();
        boolean deleted = dao.delete(System.currentTimeMillis());
        assertFalse(deleted);
    }

    @Test
    public void get() throws Exception {
        MoneyTransferDao dao = new MoneyTransferDao();
        long transferId = dao.add(
                new MoneyTransfer("EUR", 120, 1234, 3214, "My first transfer"));
        assertTrue(transferId <= System.currentTimeMillis());
        MoneyTransfer transfer = dao.get(transferId);
        assertNotNull(transfer);
        assertEquals(transfer.getCurrency(), "EUR");
        assertEquals(transfer.getAmount(), 120, 0.001);
        assertEquals(transfer.getSenderId(), 1234);
        assertEquals(transfer.getRecipientId(), 3214);
        assertEquals(transfer.getDescription(), "My first transfer");
    }

    @Test
    public void getAllEmpty() throws Exception {
        MoneyTransferDao dao = new MoneyTransferDao();
        List<MoneyTransfer> all = dao.getAll();
        assertTrue(all.isEmpty());
    }

    @Test
    public void getAllNotEmpty() throws Exception {
        MoneyTransferDao dao = new MoneyTransferDao();
        long transferId = dao.add(
                new MoneyTransfer("EUR", 120, 1234, 3214, "My first transfer"));
        assertTrue(transferId <= System.currentTimeMillis());
        List<MoneyTransfer> all = dao.getAll();
        assertFalse(all.isEmpty());
        assertEquals(all.size(), 1);
        MoneyTransfer transfer = all.get(0);
        assertEquals(transfer.getCurrency(), "EUR");
        assertEquals(transfer.getAmount(), 120, 0.001);
        assertEquals(transfer.getSenderId(), 1234);
        assertEquals(transfer.getRecipientId(), 3214);
        assertEquals(transfer.getDescription(), "My first transfer");
    }
}