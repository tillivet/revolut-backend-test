# Revolut Backend test solution

## How to build/run the code

1. Download the code by executing:

`git clone git@bitbucket.org:tillivet/revolut-backend-test.git`

2. I used maven to build the project, so building is easy:

`mvn clean package`

3. Run the project:

`java -jar target/backend-test-1.0-SNAPSHOT.jar`

## Explicit Requirements 

1. You can use Java, Scala or Kotlin
 - **I used java 1.8**
2. Keep it simple and to the point (e.g. no need to implement any authentication).
 - **No authentication :-)**
 - **I assumed that my service won't be implemnting balance of the user, so I left this check empty**
3. Assume the API is invoked by multiple systems and services on behalf of end users.
 - **undertow uses NIO - faster than blocking IO, no context switching and yet for this size of server not complex**
4. You can use frameworks/libraries if you like (except Spring), but don't forget about
requirement #2 – keep it simple and avoid heavy frameworks.
 - **I have used undertow embedded application server (for details of the choice please see next point)**
 - **I have also chosen to use Jackson library for json serialization/deserialization (also see the next point for details)**
5. The datastore should run in-memory for the sake of this test.
 - **In-memory it is**
6. The final result should be executable as a standalone program (should not require
a pre-installed container/server).
 - **maven-shade-plugin is used to put everything in one jar to execute**
 - **as you can see undertow is easy to embed**
7. Demonstrate with tests that the API works as expected.
 - **I've added one junit test for DAO**
 - **It appeared testing undertow is not easy (because of final classes in handlers),
 so I wrote integration test that starts the whole application server and usese RESTeasy library to run requests**

## Frameworks used and why

1. **Undertow**

At first, I thought about writing in plain java (as requirement is KISS and frameworks are not welcome).
But the modern features of application servers like NIO, websockets and http/2 support are really great. 
So, I decided to search for an application server

As there was a requirement running everything in a standalone jar -> then it should be embeddable application server

As API will be invoked by multiple systems and services -> most probably we would like to have a fast application server

I went to https://www.techempower.com/benchmarks/#section=data-r17&hw=ph&test=plaintext
and took notice of all java application servers. 
After checking all of them on github for number of commits and contributors / time they exits 
/ amount of features and bugs / size of the container (if available) ->
I've chosen three more or less stable and fast solutions:
 - vert.x
 - rapidoid
 - undertow
 
vert.x looked like the most promising to me, because they bragged on their website about 640KB size only :-)

I've written hello-world RESTful applications in all those three technologies and built jars. 
The sizes of jars were:
- vert.x ~ 6Mb
- rapidoid ~ 19Mb
- undertow ~ 3Mb

So, I decided to choose undertow. Apart from that that was the most mature project on Apache 2 Licence.

2. **Jackson**

I decided to use Fasterxml Jackson library because of following reasons:
- it adds only ~1Mb to the final jar
- json is the most common format for the payload and easy to read for a human
- we don't need to use some plaintext format with separators for the payload and we don't need to parse JSON manually
- it keeps the code simple to read and easy to extend API's in the future

3. **JUnit and Resteasy**

I added junit and resteasy dependency to test the app.
Those dependencies are not included into the final jar.
It appeared not easy to write JUnit tests for Undertow (i.e. HttpServerExchange is final class), so I decided to write integration tests.
I use Resteasy for integration tests for creating RESt client and testing responses with a real server started.